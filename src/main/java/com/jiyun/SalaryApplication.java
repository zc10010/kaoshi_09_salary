package com.jiyun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.jiyun.mapper")
public class SalaryApplication {

    
    public static void main(String[] args) {
        SpringApplication.run(SalaryApplication.class,args);
    }

}
