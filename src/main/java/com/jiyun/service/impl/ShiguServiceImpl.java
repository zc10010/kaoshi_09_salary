package com.jiyun.service.impl;

import com.jiyun.dto.ShiguDto;
import com.jiyun.entity.Shigu;
import com.jiyun.mapper.ShiguMapper;
import com.jiyun.service.IShiguService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@Service
public class ShiguServiceImpl extends ServiceImpl<ShiguMapper, Shigu> implements IShiguService {

    @Autowired
    ShiguMapper shiguMapper;
    @Override
    public List<ShiguDto> findAll() {
        return shiguMapper.findAll();
    }

    @Override
    public Integer findAllKoukuanByEid(Integer id) {
        return shiguMapper.findAllKoukuanByEid(id);
    }
}
