package com.jiyun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SalaryConstructor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    private String jibengongzi;

    private String jiaotongbuzhu;

    private String wucanbuzhu;

    private String jiangjin;

    private Date qiyongshijian;

    private String yanglaojinbili;

    private String yanglaojinjishu;

    private String yiliaobaoxianbili;

    private String yiliaobaoxianjishu;

    private String gongjijinbili;

    private String gongjijinjishu;

    private String deptIds;//1,2


}
