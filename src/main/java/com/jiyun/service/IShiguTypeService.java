package com.jiyun.service;

import com.jiyun.entity.ShiguType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
public interface IShiguTypeService extends IService<ShiguType> {

}
