package com.jiyun.mapper;

import com.jiyun.entity.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
