package com.jiyun.service.impl;

import com.jiyun.entity.ShiguType;
import com.jiyun.mapper.ShiguTypeMapper;
import com.jiyun.service.IShiguTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@Service
public class ShiguTypeServiceImpl extends ServiceImpl<ShiguTypeMapper, ShiguType> implements IShiguTypeService {

}
