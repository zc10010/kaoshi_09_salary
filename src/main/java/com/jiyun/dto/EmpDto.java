package com.jiyun.dto;

import com.jiyun.entity.Emp;
import com.jiyun.entity.SalaryConstructor;
import lombok.Data;

@Data
public class EmpDto extends Emp {

    private String deptNames;
    private SalaryConstructor salaryConstructor;

}
