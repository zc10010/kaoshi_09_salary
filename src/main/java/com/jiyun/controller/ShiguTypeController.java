package com.jiyun.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.jiyun.entity.ShiguType;
import com.jiyun.service.IShiguTypeService;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@RestController
@RequestMapping("/shigu-type")
public class ShiguTypeController {

    @Autowired
    IShiguTypeService shiguTypeService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addShiguType(@RequestBody ShiguType shiguType){
        shiguTypeService.save(shiguType);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateShiguType(@RequestBody ShiguType shiguType){
        shiguTypeService.updateById(shiguType);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        shiguTypeService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        shiguTypeService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public ShiguType findById(@RequestParam Integer id){
        return shiguTypeService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<ShiguType> findAll(){
        return shiguTypeService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<ShiguType> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<ShiguType> shiguTypePage = new Page<>(page, pageSize);
        return shiguTypeService.page(shiguTypePage);
    }

}
