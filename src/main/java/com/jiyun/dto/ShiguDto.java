package com.jiyun.dto;

import com.jiyun.entity.Shigu;
import lombok.Data;

@Data
public class ShiguDto extends Shigu {

    private String worknum;
    private String empName;
    private String shiguTypeName;
    private String money;


}
