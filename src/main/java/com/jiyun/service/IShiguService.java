package com.jiyun.service;

import com.jiyun.dto.ShiguDto;
import com.jiyun.entity.Shigu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
public interface IShiguService extends IService<Shigu> {

    List<ShiguDto> findAll();

    Integer findAllKoukuanByEid(Integer id);
}
