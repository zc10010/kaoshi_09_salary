/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : 09_kaoshi_salary

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2023-03-13 17:07:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('1', '人事部', '0');
INSERT INTO `dept` VALUES ('2', '薪资核算部门', '1');
INSERT INTO `dept` VALUES ('3', '招聘部门', '1');
INSERT INTO `dept` VALUES ('4', '员工培训部', '1');
INSERT INTO `dept` VALUES ('5', '研发部', '0');
INSERT INTO `dept` VALUES ('6', '深圳项目部', '5');
INSERT INTO `dept` VALUES ('7', '北京项目部', '5');
INSERT INTO `dept` VALUES ('8', '市场部', '0');
INSERT INTO `dept` VALUES ('9', '华北市场部', '8');
INSERT INTO `dept` VALUES ('10', '华南市场部', '8');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `worknum` varchar(255) DEFAULT NULL,
  `dept_ids` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES ('1', 'hr', '0001', '1,2', '女', '11111111111');
INSERT INTO `emp` VALUES ('2', '程序员1', '00099', '5,6', '男', '22222222222');
INSERT INTO `emp` VALUES ('3', '市场经理', '0005', '8,9', '男', '33333333333');

-- ----------------------------
-- Table structure for salary_constructor
-- ----------------------------
DROP TABLE IF EXISTS `salary_constructor`;
CREATE TABLE `salary_constructor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `jibengongzi` varchar(255) DEFAULT NULL,
  `jiaotongbuzhu` varchar(255) DEFAULT NULL,
  `wucanbuzhu` varchar(255) DEFAULT NULL,
  `jiangjin` varchar(255) DEFAULT NULL,
  `qiyongshijian` datetime DEFAULT NULL,
  `yanglaojinbili` varchar(255) DEFAULT NULL,
  `yanglaojinjishu` varchar(255) DEFAULT NULL,
  `yiliaobaoxianbili` varchar(255) DEFAULT NULL,
  `yiliaobaoxianjishu` varchar(255) DEFAULT NULL,
  `gongjijinbili` varchar(255) DEFAULT NULL,
  `gongjijinjishu` varchar(255) DEFAULT NULL,
  `dept_ids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of salary_constructor
-- ----------------------------
INSERT INTO `salary_constructor` VALUES ('1', '研发部门薪资结构-北京', '10000', '2000', '1000', '20000', '2023-03-10 14:58:23', '0.1', '4000', '0.1', '5000', '0.12', '8000', '5,6');
INSERT INTO `salary_constructor` VALUES ('2', '研发部门薪资结构-深圳', '9000', '1000', '500', '10000', '2023-03-10 14:58:23', '0.1', '4000', '0.1', '5000', '0.12', '8000', '5,7');
INSERT INTO `salary_constructor` VALUES ('3', '市场部-华北', '3000', '1000', '500', '50000', '2023-03-10 14:58:23', '0.1', '4000', '0.1', '5000', '0.12', '8000', '8,9');
INSERT INTO `salary_constructor` VALUES ('4', '人资薪资结构', '4000', '200', '10', '10', '2023-03-13 15:08:58', '0.01', '200', '0.01', '300', '0.01', '500', '1,2');

-- ----------------------------
-- Table structure for shigu
-- ----------------------------
DROP TABLE IF EXISTS `shigu`;
CREATE TABLE `shigu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL,
  `st_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shigu
-- ----------------------------
INSERT INTO `shigu` VALUES ('1', '2', '1', '2023-03-15 15:01:27');
INSERT INTO `shigu` VALUES ('2', '1', '2', '2023-03-01 15:01:33');
INSERT INTO `shigu` VALUES ('3', '2', '1', '2023-03-13 15:45:15');
INSERT INTO `shigu` VALUES ('4', '3', '3', '2023-03-13 15:45:24');

-- ----------------------------
-- Table structure for shigu_type
-- ----------------------------
DROP TABLE IF EXISTS `shigu_type`;
CREATE TABLE `shigu_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `money` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shigu_type
-- ----------------------------
INSERT INTO `shigu_type` VALUES ('1', '迟到', '10');
INSERT INTO `shigu_type` VALUES ('2', '早退', '50');
INSERT INTO `shigu_type` VALUES ('3', '殴打保安', '200');
INSERT INTO `shigu_type` VALUES ('4', '不带工牌', '50');
