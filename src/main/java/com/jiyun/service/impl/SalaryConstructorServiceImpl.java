package com.jiyun.service.impl;

import com.jiyun.entity.SalaryConstructor;
import com.jiyun.mapper.SalaryConstructorMapper;
import com.jiyun.service.ISalaryConstructorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@Service
public class SalaryConstructorServiceImpl extends ServiceImpl<SalaryConstructorMapper, SalaryConstructor> implements ISalaryConstructorService {

}
