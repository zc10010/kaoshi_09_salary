package com.jiyun.controller;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Dept;
import com.jiyun.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@RestController
@RequestMapping("/dept")
public class DeptController {

    @Autowired
    IDeptService deptService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addDept(@RequestBody Dept dept){
        deptService.save(dept);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateDept(@RequestBody Dept dept){
        deptService.updateById(dept);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        deptService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        deptService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Dept findById(@RequestParam Integer id){
        return deptService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Dept> findAll(){
        return deptService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Dept> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<Dept> deptPage = new Page<>(page, pageSize);
        return deptService.page(deptPage);
    }


    /**
     * [
     *  {
     *      value:"",
     *      label:"",
     *      children:[
     *
     *      ]
     *  }
     * ]
     * @return
     */
    @RequestMapping("findDeptList")
    public List<Map> findDeptList(){

        List<Dept> deptList = deptService.list();

        return findSonByPid(deptList,0);
    }

    public List<Map> findSonByPid(List<Dept> deptList,Integer pid){
        ArrayList<Map> list = new ArrayList<>();
        for (Dept dept : deptList) {
            if (dept.getPid().equals(pid)) {
                HashMap hashMap = new HashMap();
                hashMap.put("value",dept.getId());
                hashMap.put("label",dept.getName());
                List<Map> son = findSonByPid(deptList, dept.getId());
                if(CollectionUtils.isNotEmpty(son)){
                    hashMap.put("children",son);
                }
                list.add(hashMap);
            }
        }
        return list;
    }

}
