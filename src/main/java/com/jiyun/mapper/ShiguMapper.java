package com.jiyun.mapper;

import com.jiyun.dto.ShiguDto;
import com.jiyun.entity.Shigu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
public interface ShiguMapper extends BaseMapper<Shigu> {

    @Select("select \n" +
            "sg.*,e.name as empName,e.worknum as worknum,\n" +
            "st.name as shiguTypeName,st.money as money\n" +
            "from \n" +
            "shigu sg,\n" +
            "emp e ,shigu_type st\n" +
            "where sg.eid=e.id and sg.st_id=st.id")
    List<ShiguDto> findAll();


    @Select("select \n" +
            "sum(money)\n" +
            "from \n" +
            "shigu sg,\n" +
            "emp e ,shigu_type st\n" +
            "where sg.eid=e.id and sg.st_id=st.id and sg.eid=#{id}\n" +
            "GROUP BY sg.eid")
    Integer findAllKoukuanByEid(@Param("id") Integer id);
}
