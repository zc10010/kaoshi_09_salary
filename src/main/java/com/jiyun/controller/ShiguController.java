package com.jiyun.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.dto.ShiguDto;
import com.jiyun.entity.Shigu;
import com.jiyun.service.IShiguService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@RestController
@RequestMapping("/shigu")
public class ShiguController {

    @Autowired
    IShiguService shiguService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addShigu(@RequestBody Shigu shigu){
        shigu.setCreateTime(new Date());
        shiguService.save(shigu);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateShigu(@RequestBody Shigu shigu){
        shiguService.updateById(shigu);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        shiguService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        shiguService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Shigu findById(@RequestParam Integer id){
        return shiguService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<ShiguDto> findAll(){
        return shiguService.findAll();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Shigu> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<Shigu> shiguPage = new Page<>(page, pageSize);
        return shiguService.page(shiguPage);
    }

}
