package com.jiyun.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.dto.SalaryConstructorDto;
import com.jiyun.entity.Dept;
import com.jiyun.entity.SalaryConstructor;
import com.jiyun.service.IDeptService;
import com.jiyun.service.ISalaryConstructorService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-03-10
 */
@RestController
@RequestMapping("/salary-constructor")
public class SalaryConstructorController {

    @Autowired
    ISalaryConstructorService salaryConstructorService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addSalaryConstructor(@RequestBody SalaryConstructor salaryConstructor){
        salaryConstructor.setQiyongshijian(new Date());
        salaryConstructorService.save(salaryConstructor);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateSalaryConstructor(@RequestBody SalaryConstructor salaryConstructor){
        salaryConstructorService.updateById(salaryConstructor);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        salaryConstructorService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        salaryConstructorService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public SalaryConstructor findById(@RequestParam Integer id){
        return salaryConstructorService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<SalaryConstructor> findAll(){
        return salaryConstructorService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<SalaryConstructorDto> findPage(@RequestParam Integer page, @RequestParam Integer pageSize){
        Page<SalaryConstructor> salaryConstructorPage = new Page<>(page, pageSize);
        salaryConstructorService.page(salaryConstructorPage);


        Page<SalaryConstructorDto> salaryConstructorDtoPage = new Page<>(page, pageSize);
        salaryConstructorDtoPage.setTotal(salaryConstructorPage.getTotal());

        List<SalaryConstructor> salaryConstructorList = salaryConstructorPage.getRecords();

        //数据转换成这个list
        ArrayList<SalaryConstructorDto> salaryConstructorDtos = new ArrayList<>();
        //转换过程
        for (SalaryConstructor salaryConstructor : salaryConstructorList) {
            SalaryConstructorDto salaryConstructorDto = new SalaryConstructorDto();
            BeanUtils.copyProperties(salaryConstructor,salaryConstructorDto);

            String deptNames = "";
            for (String did : salaryConstructor.getDeptIds().split(",")) {
                Dept dept = deptService.getById(did);
                deptNames+=dept.getName()+"-";
            }
            deptNames = deptNames.substring(0,deptNames.length()-1);

            salaryConstructorDto.setDeptNames(deptNames);

            salaryConstructorDtos.add(salaryConstructorDto);
        }

        salaryConstructorDtoPage.setRecords(salaryConstructorDtos);
        return salaryConstructorDtoPage;
    }

    @Autowired
    IDeptService deptService;

}
